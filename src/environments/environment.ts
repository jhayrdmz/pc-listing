// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA9IX-kmqoyJA9PuZujhCsiWhW0eFKQo98",
    authDomain: "sampleapp-8450b.firebaseapp.com",
    databaseURL: "https://sampleapp-8450b.firebaseio.com",
    projectId: "sampleapp-8450b",
    storageBucket: "sampleapp-8450b.appspot.com",
    messagingSenderId: "565458155841",
    appId: "1:565458155841:web:15162b9ec430e7e6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
