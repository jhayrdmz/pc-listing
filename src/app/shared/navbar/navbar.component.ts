import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(
    public afAuth: AngularFireAuth,
    public dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit() {
  }

  showLogin() {
    this.dialog.open(LoginComponent, {
      width: '700px',
      height: '500px',
      panelClass: 'no-padding-dialog'
    });
  }

  logout() {
    this.afAuth.auth.signOut().finally(() => {
      this.router.navigate(['/']);
    });
  }

}
