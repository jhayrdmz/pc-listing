import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  processing: boolean;
  loginError: any;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    public dialogRef: MatDialogRef<LoginComponent>
  ) {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
  }

  login() {
    if(this.loginForm.invalid || this.processing) {
      return;
    }

    this.processing = true;

    this.auth.login(
      this.loginForm.value.email,
      this.loginForm.value.password
    ).then(() => {
      this.dialogRef.close(true);
    }).catch((error: any) => {
      this.loginError = error;
    }).finally(() => {
      this.processing = false;
    });
  }

  ngOnInit() {
  }

}
