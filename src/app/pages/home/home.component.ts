import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from '../../services/category/category.service';
import { OwlCarousel } from 'ngx-owl-carousel';
import { ItemsService } from '../../services/item/items.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild('owlElement') owlElement: OwlCarousel;
  @ViewChild('owlElementCategories') owlElementCategories: OwlCarousel;
  
  categories: any[];
  featured_items: any[];
  new_items: any[];

  constructor(
    public category: CategoryService,
    public itemService: ItemsService
  ) { }

  ngOnInit() {
    this.categories = this.category.getCategories();

    this.itemService.getFeaturedItems().then(doc => {
      this.featured_items = doc.map(doc => doc.data());
    });

    this.itemService.getNewItems().then(doc => {
      this.new_items = doc.map(doc => {
        const data = doc.data();
        const id = doc.id;
        return { id, ...data };
      });
    })
  }

  owlNext() {
    this.owlElementCategories.next([200]);
  }

  owlPrev() {
    this.owlElementCategories.previous([200]);
  }

}
