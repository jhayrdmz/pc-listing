import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { AuthService } from '../../../services/auth/auth.service';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';

export class PasswordErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | null): boolean {
    return control.parent.errors && control.parent.errors['notSame'];
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  processing: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) {
    this.registerForm = this.formBuilder.group({
      displayName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      confirm_password: new FormControl()
    },{ validators: this.checkPasswords });
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirm_password = group.controls.confirm_password.value;

    return pass === confirm_password ? null : { notSame: true };
  }

  register() {
    if(this.registerForm.invalid || this.processing) {
      return;
    }

    this.processing = true;

    this.auth.register(
      this.registerForm.value.email,
      this.registerForm.value.password,
      this.registerForm.value.displayName
    ).then(() => {
      this.router.navigate(['/'])
    }).catch((error: any) => {
      console.log(error);
    }).finally(() => {
      this.processing = false;
    });
  }

  ngOnInit() {
  }

}
