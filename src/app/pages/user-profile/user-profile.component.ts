import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  userInfo: any;
  isLoaded: boolean;

  featured_items = [
    {
      item_name: 'Lenovo Thinkpad E560 Core i5 6200u 8GB 500GB AMD R7 M370 2GB',
      item_price: 24500.00,
      image_url: 'https://picsum.photos/240/120?random=1'
    },{
      item_name: 'Atx mobo 3rd gen Z77 - MSI Z77A-G41 with backplate',
      item_price: 3200.00,
      image_url: 'https://picsum.photos/240/120?random=2'
    },{
      item_name: 'Gigabyte ga-h81m-d2v',
      item_price: 1500.00,
      image_url: 'https://picsum.photos/240/120?random=3'
    },{
      item_name: 'Intel® Core™ i5-4670K Processor (6M Cache, up to 3.80 GHz)',
      item_price: 4800.00,
      image_url: 'https://picsum.photos/240/120?random=4'
    }, {
      item_name: 'This is just a test',
      item_price: 4200.00,
      image_url: 'https://picsum.photos/240/120?random=5'
    }
  ];

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.auth.getUserInfoByDisplayName(params['username']).then(res => {
        this.userInfo = res[0];
        this.isLoaded = true;
      });
    });
  }

}
