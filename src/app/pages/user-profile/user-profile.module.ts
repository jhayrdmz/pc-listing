import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile.component';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { ItemModule } from '../../components/item/item.module';
import { ReviewModule } from '../../components/review/review.module';
import { SharedModule } from '../../shared.module';

@NgModule({
  declarations: [
    UserProfileComponent
  ],
  imports: [
    CommonModule,
    UserProfileRoutingModule,
    MatProgressSpinnerModule,
    NgxSkeletonLoaderModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule,
    ItemModule,
    ReviewModule,
    SharedModule
  ]
})
export class UserProfileModule { }
