import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostItemComponent } from './post-item.component';
import { PostItemRoutingModule } from './post-item-routing.module';

@NgModule({
  declarations: [PostItemComponent],
  imports: [
    CommonModule,
    PostItemRoutingModule
  ]
})
export class PostItemModule { }
