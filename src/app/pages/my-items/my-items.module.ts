import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyItemsComponent } from './my-items.component';
import { MyItemsRoutingModule } from './my-items-routing.module';

@NgModule({
  declarations: [MyItemsComponent],
  imports: [
    CommonModule,
    MyItemsRoutingModule
  ]
})
export class MyItemsModule { }
