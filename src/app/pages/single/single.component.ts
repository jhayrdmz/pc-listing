import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ItemsService } from '../../services/item/items.service';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.scss']
})
export class SingleComponent implements OnInit {

  item: any;
  relatedItems: any;

  constructor(
    private _location: Location,
    private route : ActivatedRoute,
    public itemService: ItemsService
  ) { }

  back() {
    this._location.back();
  }

  getItemInfo(id: any) {
    this.itemService.getItem(id).then(doc => {
      this.item = doc.data();
      this.getRelatedItems(this.item.category);
    });
  }

  getRelatedItems(category: string) {
    this.itemService.getRelatedItems(category).then(snapshot => {
      this.relatedItems = snapshot.map(doc => {
        const data = doc.data();
        const id = doc.id;
        return { id, ...data };
      });
    })
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getItemInfo(params.id);
    })
  }

}
