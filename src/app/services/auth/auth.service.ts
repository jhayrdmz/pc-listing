import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private userCollection: AngularFirestoreCollection;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore
  ) {
    this.userCollection = this.afs.collection(`users`);

    this.afAuth.authState.subscribe(res => {
      if(res) {
        let userDoc = this.userCollection.doc(res.uid);
        this.userCollection.doc(res.uid).valueChanges().subscribe(user => {
          userDoc.set({
            // uid: res.uid,
            displayName: res.displayName,
            email: res.email,
            photoURL: res.photoURL,
            providerId: res.providerData[0].providerId,
            providerUid: res.providerData[0].uid,
            creationTime: res.metadata.creationTime,
            lastSignInTime: res.metadata.lastSignInTime
          });
        });
      }
    });
  }

  get currentUserObservable(): any {
    return this.afAuth.authState;
  }

  login(email: string, password: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(
        email, password
      ).then(resolve).catch(reject);
    });
  }

  register(email: string, password: string, displayName: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(
        email, password
      ).then(() => {
        this.afAuth.auth.currentUser.updateProfile({
          displayName: displayName,
          photoURL: `https://api.adorable.io/avatars/285/${displayName}_${Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 32)}.png`
        }).finally(resolve);
      }).catch(reject);
    });
  }

  getUserInfoByDisplayName(displayName: string) {
    return new Promise(resolve => {
      this.afs.collection('users', ref => {
        let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
        query = query.where('displayName', '==', displayName);
        return query;
      }).valueChanges().subscribe(resolve);
    });
  }

}
