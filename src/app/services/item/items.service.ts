import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private afs: AngularFirestore) {
  }

  async getItem(id: string) {
    const item = await this.afs.firestore
      .collection('items')
      .doc(id)
      .get();
    return item;
  }

  async getFeaturedItems() {
    const featuredItems = await this.afs.firestore
      .collection('items')
      .where('featured', '==', true)
      .get();
    return featuredItems.docs;
  }

  async getNewItems() {
    const newItems = await this.afs.firestore
      .collection('items')
      .orderBy('createdAt')
      .limit(12)
      .get();
    return newItems.docs;
  }

  async getRelatedItems(category: string) {
    const relatedItems = await this.afs.firestore
      .collection('items')
      .where('category', '==', category)
      .limit(8)
      .get();
    return relatedItems.docs;
  }

}
