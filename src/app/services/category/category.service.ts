import { Injectable } from '@angular/core';

export interface CategoryElement {
  name: string,
  icon: string
}

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor() { }

  getCategories() {
    return [
      { name: 'Case', icon: 'assets/icons/case.svg' },
      { name: 'Processor', icon: 'assets/icons/cpu.svg' },
      { name: 'Graphics Card', icon: 'assets/icons/graphics-card.svg' },
      { name: 'Motherboard', icon: 'assets/icons/motherboard.svg' },
      { name: 'CPU Fan', icon: 'assets/icons/fan.svg' },
      { name: 'Monitor', icon: 'assets/icons/monitor.svg' },
      { name: 'Keyboard', icon: 'assets/icons/keyboard.svg' },
      { name: 'Mouse', icon: 'assets/icons/mouse.svg' }
    ];
  }

}
