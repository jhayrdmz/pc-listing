import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ItemComponent } from './item.component';
import { MatIconModule } from '@angular/material/icon';

import { SharedModule } from '../../shared.module';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    RouterModule,
    SharedModule
  ],
  declarations: [ItemComponent],
  exports: [ItemComponent]
})
export class ItemModule { }
