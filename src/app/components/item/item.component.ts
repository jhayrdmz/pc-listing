import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnChanges {

  @Input() item: Object;
  @Input() user: boolean = true;

  constructor() { }

  ngOnChanges() {
  }

}
