import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

import { HomeComponent } from './pages/home/home.component';
import { SingleComponent } from './pages/single/single.component';
import { SearchComponent } from './pages/search/search.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'search/:keyword', component: SearchComponent },
  { path: 'item/:id', component: SingleComponent },
  {
    path: 'register',
    loadChildren: () => import('./pages/auth/register/register.module')
      .then(mod => mod.RegisterModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/auth/forgot-password/forgot-password.module')
      .then(mod => mod.ForgotPasswordModule)
  },
  {
    path: 'post-item',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/post-item/post-item.module')
      .then(mod => mod.PostItemModule)
  },
  {
    path: 'messages',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/messages/messages.module')
      .then(mod => mod.MessagesModule)
  },
  {
    path: 'my-items',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/my-items/my-items.module')
      .then(mod => mod.MyItemsModule)
  },
  {
    path: 'account-settings',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/account-settings/account-settings.module')
      .then(mod => mod.AccountSettingsModule)
  },
  {
    path: ':username',
    loadChildren: () => import('./pages/user-profile/user-profile.module')
      .then(mod => mod.UserProfileModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
