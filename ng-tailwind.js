module.exports = {
  // Tailwind Paths
  configJS: '/Users/dev/Documents/apps/tpc/tailwind.config.js',
  sourceCSS: '/Users/dev/Documents/apps/tpc/src/tailwind-src.css',
  outputCSS: '/Users/dev/Documents/apps/tpc/src/tailwind.css',
  // Sass
  sass: false,
  // PurgeCSS Settings
  purge: false,
  keyframes: false,
  fontFace: false,
  rejected: false,
  whitelist: [],
  whitelistPatterns: [],
  whitelistPatternsChildren: [],
  extensions: [
    '.ts',
    '.html',
    '.js'
  ]
}
