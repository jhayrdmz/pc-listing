module.exports = {
  theme: {
    extend: {
      colors: {
        'primary': '#0077FF',
        'primary-light': '#73b2fb'
      },
      fontSize: {
        'xxs': '0.625rem'
      },
      screens: {
        '2xl': '1366px'
      }
    }
  },
  variants: {
    opacity: ['responsive', 'hover']
  },
  plugins: []
}
